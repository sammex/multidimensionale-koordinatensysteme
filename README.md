# README #

## How to run the code ##
* install "The Haskell Platform" [here](https://www.haskell.org/platform/)
* go to the directory "CODE"
* type ``ghci`` and then, in ghci, type ``:l SOMC.hs``
* now you can play with the code! You can invoke the main function (which is not useful at the moment) by typing ``main`` in ghci

## What is this repository for? ##
* grouping data
* pre-process data

## Contribution guidelines ##
* make sure the code compiles fine and has no bugs!

### Who do I talk to? ###
* repo owner or admin
* visit the wiki
